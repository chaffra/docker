.. Description of the FEniCS Docker images

List of FEniCS Docker images
============================

Please see `our Bitbucket repository
<https://bitbucket.org/fenics-project/docker>`_ for a full list.
